package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import javax.swing.JOptionPane;
import javax.swing.border.EmptyBorder;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class TambahMahasiswaGUI{

    public TambahMahasiswaGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        // Judul Utama
        JLabel titleLabel = new JLabel("Tambah Mahasiswa");
        titleLabel.setBorder(new EmptyBorder(10,0,10,0));
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);

        // Label
        JLabel labelNama = new JLabel("Nama:");
        labelNama.setHorizontalAlignment(JLabel.CENTER);
        labelNama.setBorder(new EmptyBorder(10,0,0,0));
        JTextField fieldNama = new JTextField();
        JLabel labelNPM = new JLabel("NPM:");
        labelNPM.setHorizontalAlignment(JLabel.CENTER);
        labelNPM.setBorder(new EmptyBorder(10,0,0,0));
        JTextField fieldNPM = new JTextField();

        // Button
        JButton tambah = new JButton("Tambahkan");
        tambah.setBackground(new Color(156, 198, 52));
        tambah.setForeground(Color.white);
        JButton kembali = new JButton("Kembali");
        kembali.setBackground(new Color(132, 174, 204));
        kembali.setForeground(Color.white);

        // SubPanel
        JPanel tambahMahasiswa = new JPanel(new GridLayout(10,1, 0, 10));
        tambahMahasiswa.setBackground(Color.WHITE);
        tambahMahasiswa.setPreferredSize(new Dimension(300,360));
        tambahMahasiswa.add(labelNama);
        tambahMahasiswa.add(fieldNama);
        tambahMahasiswa.add(labelNPM);
        tambahMahasiswa.add(fieldNPM);
        tambahMahasiswa.add(tambah);
        tambahMahasiswa.add(kembali);

        JPanel container = new JPanel();
        container.setBackground(Color.WHITE);
        container.add(titleLabel);
        container.add(tambahMahasiswa);
        frame.add(container);
        frame.revalidate();

        tambah.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent event){
                String nama = fieldNama.getText();
                String npm = fieldNPM.getText();
                if (nama.isEmpty() || npm.isEmpty()) {
                    JOptionPane.showMessageDialog(frame, "Mohon isi seluruh Field");
                } else if (getMahasiswa(Long.parseLong(npm), daftarMahasiswa) != null) {
                    JOptionPane.showMessageDialog(frame, "NPM " + npm + " sudah pernah ditambahkan sebelumnya");
                    fieldNama.setText("");
                    fieldNPM.setText("");
                } else {
                    daftarMahasiswa.add(new Mahasiswa(nama, Long.parseLong(npm)));
                    JOptionPane.showMessageDialog(frame, "Mahasiswa " + npm + "-" + nama + " berhasil ditambahkan");
                    fieldNama.setText("");
                    fieldNPM.setText("");
                }
            }
        });

        kembali.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent event){
                frame.remove(container);
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
            }
        });
    }

    private Mahasiswa getMahasiswa(long npm, ArrayList<Mahasiswa> daftarMahasiswa) {
        for (Mahasiswa mahasiswa : daftarMahasiswa) {
            if (mahasiswa.getNpm() == npm){
                return mahasiswa;
            }
        }
        return null;
    }
}
