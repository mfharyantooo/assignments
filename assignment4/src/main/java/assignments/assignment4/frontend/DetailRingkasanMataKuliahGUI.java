package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class DetailRingkasanMataKuliahGUI {
    public DetailRingkasanMataKuliahGUI(JFrame frame, MataKuliah mataKuliah, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        // Mengatur font
        Font fontGeneral = new Font("SansSerif", Font.PLAIN, 13);
        Font fontBold = new Font("SansSerif", Font.BOLD, 12);

        // Judul Utama
        JLabel titleLabel = new JLabel("Detail Ringkasan Mata Kuliah");
        titleLabel.setBorder(new EmptyBorder(10,0,10,0));
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);

        // Mengatur Label
        JLabel nama = new JLabel("Nama mata kuliah: " + mataKuliah.getNama());
        nama.setFont(fontGeneral);
        JLabel kode = new JLabel("Kode: " + mataKuliah.getKode());
        kode.setFont(fontGeneral);
        JLabel sks = new JLabel("SKS: " + mataKuliah.getSKS());
        sks.setFont(fontGeneral);
        JLabel jumlahMahasiswa = new JLabel("Jumlah mahasiswa: " + mataKuliah.getJumlahMahasiswa());
        jumlahMahasiswa.setFont(fontGeneral);
        JLabel kapasitas = new JLabel("Kapasitas: " + mataKuliah.getKapasitas());
        kapasitas.setFont(fontGeneral);
        JLabel daftar = new JLabel("Daftar Mahasiswa:");
        daftar.setFont(fontGeneral);

        // Mengatur Button
        JButton selesai = new JButton("Selesai");
        selesai.setBackground(new Color(156, 198, 52));
        selesai.setForeground(Color.white);
        selesai.setHorizontalAlignment(JButton.CENTER);
        selesai.setPreferredSize(new Dimension(150, 30));

        // Mengatur ukuran panel sesuai jumlah informasi
        int jumlahBaris = 7 + Math.max(1, mataKuliah.getJumlahMahasiswa());
        if (mataKuliah.getJumlahMahasiswa() >= 8) {
            jumlahBaris = jumlahBaris - mataKuliah.getJumlahMahasiswa() + 1;
        }
        frame.setSize(500, Math.max(jumlahBaris*25 + 170, 500));
        JPanel detailMataKuliah = new JPanel(new GridLayout(jumlahBaris,1));
        detailMataKuliah.setBackground(Color.WHITE);
        detailMataKuliah.setPreferredSize(new Dimension(350, jumlahBaris*25));

        detailMataKuliah.add(nama);
        detailMataKuliah.add(kode);
        detailMataKuliah.add(sks);
        detailMataKuliah.add(jumlahMahasiswa);
        detailMataKuliah.add(kapasitas);
        detailMataKuliah.add(daftar);

        if (mataKuliah.getJumlahMahasiswa() == 0) {
            JLabel temp = new JLabel("Belum ada mahasiswa yang mengambil mata kuliah ini.");
            temp.setFont(fontBold);
            detailMataKuliah.add(temp);
        } else if (mataKuliah.getJumlahMahasiswa() >= 8) {
            Mahasiswa[] mahasiswa = mataKuliah.getDaftarMahasiswa();
            String[] daftarNama = new String[mataKuliah.getJumlahMahasiswa()];
            for (int i=0; i<mataKuliah.getJumlahMahasiswa(); i++) {
                daftarNama[i] = (i+1) + ". " + mahasiswa[i].getNama();
            }
            JComboBox<String> namaMahasiswa = new JComboBox<>(daftarNama);
            namaMahasiswa.setMaximumRowCount(mataKuliah.getJumlahMahasiswa());
            namaMahasiswa.setBackground(Color.WHITE);
            detailMataKuliah.add(namaMahasiswa);
        } else {
            Mahasiswa[] mahasiswa = mataKuliah.getDaftarMahasiswa();
            for (int i=0; i<mataKuliah.getJumlahMahasiswa(); i++) {
                JLabel namaMahasiswa = new JLabel((i+1) + ". " + mahasiswa[i].getNama());
                namaMahasiswa.setFont(fontBold);
                detailMataKuliah.add(namaMahasiswa);
            }
        }

        // SubPanel
        JPanel container = new JPanel();
        container.setBackground(Color.WHITE);
        container.add(titleLabel);
        container.add(detailMataKuliah);
        container.add(selesai);
        frame.add(container);
        frame.revalidate();

        selesai.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent event){
                frame.remove(container);
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
            }
        });
        
    }
}
