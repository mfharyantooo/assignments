package assignments.assignment1;

import java.util.Scanner;

public class ExtractNPM {

    public static boolean cekKodeNPM(String npm) {
        int komputasi = 0;

        // Komputasi = perkalian dan penjumlahan antar digit sesuai dengan permintaan soal
        for (int i=0; i<=6; i++){
            int digitPertama = Character.getNumericValue(npm.charAt(i));
            int digitKedua = Character.getNumericValue(npm.charAt(12-i));
            if (i == 6) {
                komputasi += digitPertama;
            } else {
                komputasi += digitPertama * digitKedua;
            }
        }

        // Jika hasil komputasi lebih dari 1 digit, jumlahkan semua digit hingga menjadi 1 digit
        while (komputasi >= 10) {
            int jumlahDigit = 0;
            while (komputasi != 0) {
                jumlahDigit += komputasi % 10;
                komputasi /= 10;
            }
            komputasi = jumlahDigit;
        }

        // return berupa boolean apakah kodeNPM sudah sesuai
        return komputasi == Integer.parseInt(npm.substring(13));
    }

    public static boolean validate(long npm) {
        // Mengubah tipe data npm menjadi string supaya lebih mudah diolah
        String npmString = Long.toString(npm);

        // Mengembalikan nilai false langsung jika jumlah digit tidak sama dengan 14
        if (npmString.length() != 14){
            return false;
        }

        int tahunMasuk = Integer.parseInt(npmString.substring(0,2));
        int tahunLahir = Integer.parseInt(npmString.substring(10,12));
        int selisihTahun = tahunMasuk - tahunLahir;
        int kode = Integer.parseInt(npmString.substring(2,4));

        // Melakukan validasi umur, jurusan, dan kodeNPM dengan bantuan method tambahan
        boolean umur = (selisihTahun >= 15);
        boolean jurusan = (kode==1 || kode==2 || kode==3 || kode==11 || kode==12);
        boolean kodeNPM = cekKodeNPM(npmString);

        // Mengembalikan nilai true jika umur, jurusan, dan kodeNPM valid
        return (umur && jurusan && kodeNPM);
    }

    public static String extract(long npm) {
        // Mengubah tipe data npm menjadi string supaya lebih mudah diolah
        String npmString = Long.toString(npm);

        // Menentukan jurusan yang dimaksud
        String kodeJurusan = npmString.substring(2,4);
        String jurusan;
        if (kodeJurusan.equals("01")) {
            jurusan = "Ilmu Komputer";
        } else if (kodeJurusan.equals("02")) {
            jurusan = "Sistem Informasi";
        } else if (kodeJurusan.equals("03")) {
            jurusan = "Teknologi Informasi";
        } else if (kodeJurusan.equals("11")) {
            jurusan = "Teknik Telekomunikasi";
        } else {
            jurusan = "Teknik Elektro";
        }

        // Membuat format tanggal lahir yang sesuai
        String tanggalLahir = npmString.substring(4,6);
        tanggalLahir += "-" + npmString.substring(6,8);
        tanggalLahir += "-" + npmString.substring(8,12);

        // Membuat format ekstraksi informasi yang sesuai
        String informasi = "Tahun masuk: 20" + npmString.substring(0,2);
        informasi += "\nJurusan: " + jurusan;
        informasi += "\nTanggal Lahir: " + tanggalLahir;

        return informasi;
    }

    public static void main(String args[]) {
        Scanner input = new Scanner(System.in);
        boolean exitFlag = false;
        while (!exitFlag) {
            long npm = input.nextLong();
            if (npm < 0) {
                exitFlag = true;
                break;
            }

            // Pengecekan NPM dan ekstraksi informasi
            if (!validate(npm)) {
                System.out.println("NPM tidak valid!");
            } else {
                System.out.println(extract(npm));
            }
        }
        input.close();
    }
}