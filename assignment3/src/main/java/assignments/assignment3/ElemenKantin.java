package assignments.assignment3;

class ElemenKantin extends ElemenFasilkom {

    private Makanan[] daftarMakanan = new Makanan[10];

    public ElemenKantin(String nama) {
        super(nama, "ElemenKantin");
    }

    public void setMakanan(String nama, long harga) {
        for (int i=0; i<this.daftarMakanan.length; i++) {
            if (this.daftarMakanan[i] == null) {
                this.daftarMakanan[i] = new Makanan(nama, harga);
                System.out.printf("%s telah mendaftarkan makanan %s dengan harga %d\n", this, nama, harga);
                break;
            } else if (this.daftarMakanan[i].getNama().equals(nama)) {
                System.out.printf("[DITOLAK] %s sudah pernah terdaftar\n", nama);
                break;
            }
        }
    }

    public Makanan getMakanan(String namaMakanan) {
        for (Makanan makanan: daftarMakanan) {
            if (makanan != null && makanan.getNama().equals(namaMakanan)) {
                return makanan;
            }
        }
        return null;
    }

    @Override
    public void menyapa(ElemenFasilkom elemenFasilkom) {
        if (this.telahDisapa(elemenFasilkom) == -1) {
            System.out.printf("%s menyapa dengan %s\n", this, elemenFasilkom);
            // Menambah jumlah dan daftar elemen yang telah disapa
            this.setTelahMenyapa(elemenFasilkom);
            elemenFasilkom.setTelahMenyapa(this);
        } else {
            System.out.printf("[DITOLAK] %s telah menyapa %s hari ini\n", this, elemenFasilkom);
        }
    }
}