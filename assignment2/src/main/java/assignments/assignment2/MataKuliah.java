package assignments.assignment2;

public class MataKuliah {
    private String kode;
    private String nama;
    private int sks;
    private int kapasitas;
    private Mahasiswa[] daftarMahasiswa;
    private int jumlahMahasiswa;

    public MataKuliah(String kode, String nama, int sks, int kapasitas){
        this.kode = kode;
        this.nama = nama;
        this.sks = sks;
        this.kapasitas = kapasitas;
        this.daftarMahasiswa = new Mahasiswa[kapasitas];
    }

    public String getKode(){
        return this.kode;
    }

    public String getNama(){
        return this.nama;
    }

    public int getSKS(){
        return this.sks;
    }

    public int getKapasitas(){
        return this.kapasitas;
    }

    public int getJumlahMahasiswa(){
        return this.jumlahMahasiswa;
    }

    public Mahasiswa[] getMahasiswa(){
        return this.daftarMahasiswa;
    }

    public void addMahasiswa(Mahasiswa mahasiswa){
        this.jumlahMahasiswa++;
        for (int i=0; i<this.kapasitas; i++){
            if (this.daftarMahasiswa[i] == null){
                this.daftarMahasiswa[i] = mahasiswa;
                break;
            }
        }
    }

    public void dropMahasiswa(Mahasiswa mahasiswa){
        this.jumlahMahasiswa--;
        for (int i=0; i<this.kapasitas; i++){
            if (this.daftarMahasiswa[i] == mahasiswa){
                this.daftarMahasiswa[i] = null;
                break;
            }
        }
    }

    public String toString(){
        return this.nama;
    }
}
