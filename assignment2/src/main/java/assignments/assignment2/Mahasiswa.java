package assignments.assignment2;

public class Mahasiswa {
    private MataKuliah[] mataKuliah = new MataKuliah[10];
    private String[] masalahIRS;
    private int totalSKS;
    private String nama;
    private String jurusan;
    private long npm;
    private int jumlahMataKuliah;
    private int jumlahMasalahIRS;

    public Mahasiswa(String nama, long npm){
        this.nama = nama;
        this.npm = npm;
        String npmStr = Long.toString(npm);
        this.jurusan = npmStr.substring(2,4).equals("01") ? "Ilmu Komputer" : "Sistem Informasi";
    }

    public String getNama(){
        return this.nama;
    }

    public long getNPM(){
        return this.npm;
    }

    public String getJurusan(){
        return this.jurusan;
    }

    public int getTotalSKS(){
        return this.totalSKS;
    }

    public int getJumlahMataKuliah(){
        return this.jumlahMataKuliah;
    }

    public MataKuliah[] getMataKuliah(){
        return this.mataKuliah;
    }

    public int getJumlahMasalahIRS(){
        return this.jumlahMasalahIRS;
    }

    public String[] getMasalahIRS(){
        return this.masalahIRS;
    }

    public int getIndexMataKuliah(MataKuliah mataKuliah){
        // Mengecek apakah mataKuliah sudah ada atau belum
        for (int i=0; i<this.mataKuliah.length; i++){
            if (this.mataKuliah[i] == mataKuliah){
                return i;
            }
        }
        return -1;
    }
    
    public void addMatkul(MataKuliah mataKuliah){
        int indexMataKuliah = getIndexMataKuliah(mataKuliah);

        if (indexMataKuliah != -1){
            System.out.println("[DITOLAK] " + mataKuliah + " telah diambil sebelumnya.");
        } else if (mataKuliah.getKapasitas() == mataKuliah.getJumlahMahasiswa()){
            System.out.println("[DITOLAK] " + mataKuliah + " telah penuh kapasitasnya.");
        } else if (this.jumlahMataKuliah == 10){
            System.out.println("[DITOLAK] Maksimal mata kuliah yang diambil hanya 10.");
        } else {
            for (int i=0; i<this.mataKuliah.length; i++){
                if (this.mataKuliah[i] == null){
                    mataKuliah.addMahasiswa(this);
                    this.mataKuliah[i] = mataKuliah;
                    this.totalSKS += mataKuliah.getSKS();
                    this.jumlahMataKuliah++;
                    break;
                }
            }
        }
    }

    public void dropMatkul(MataKuliah mataKuliah){
        int indexMataKuliah = getIndexMataKuliah(mataKuliah);

        if (indexMataKuliah == -1){
            System.out.println("[DITOLAK] " + mataKuliah + " belum pernah diambil.");
        } else {
            mataKuliah.dropMahasiswa(this);
            this.mataKuliah[indexMataKuliah] = null;
            this.totalSKS -= mataKuliah.getSKS();
            this.jumlahMataKuliah--;
        }
    }


    public void cekIRS(){
        if (this.jumlahMataKuliah != 0) {
            this.jumlahMasalahIRS = 0;
            this.masalahIRS = new String[20];

            if (this.totalSKS > 24){
                this.masalahIRS[this.jumlahMasalahIRS] = "SKS yang Anda ambil lebih dari 24";
                this.jumlahMasalahIRS++;
            }

            String kodeJurusan = this.jurusan.equals("Ilmu Komputer") ? "IK" : "SI";

            for (MataKuliah matkul : this.mataKuliah){
                if (matkul == null || matkul.getKode().equals("CS")){
                    continue;
                } else if (!matkul.getKode().equals(kodeJurusan)){
                    this.masalahIRS[this.jumlahMasalahIRS] = "Mata Kuliah " + matkul.getNama();
                    this.masalahIRS[this.jumlahMasalahIRS] += " tidak dapat diambil jurusan " + kodeJurusan;
                    this.jumlahMasalahIRS++;
                }
            }
        }
    }

    public String toString(){
        return this.nama;
    }
}
